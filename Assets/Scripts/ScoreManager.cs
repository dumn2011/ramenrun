﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
	public Text ScoreText;
	public Text HighScoreText;
	
	public float ScoreCount;
	public float HighScoreCount;
	public float PointsPerSecond;
	public bool ScoreIncreasing;

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.HasKey("HighScore"))
		{
			HighScoreCount = PlayerPrefs.GetFloat("HighScore");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		if (ScoreIncreasing)
		{
			ScoreCount += PointsPerSecond * Time.deltaTime;
		}

		if (ScoreCount>HighScoreCount)
		{
			HighScoreCount = ScoreCount;
			PlayerPrefs.SetFloat("HighScore", HighScoreCount);
		}
		
		ScoreText.text = "Score: " + Mathf.Round(ScoreCount);
		HighScoreText.text = "High Score: " +  Mathf.Round(HighScoreCount);
	}
	public void AddScore(int pointsToAdd)
	{
		ScoreCount += pointsToAdd;
	}
}
