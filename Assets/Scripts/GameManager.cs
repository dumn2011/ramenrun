﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
	public Transform PlatformGenerator;
	private Vector3 _platformStartPoint;

	public PlayerController ThePlayer;
	private Vector3 _playerStartPoint;

	private ObjectDestroyer[] _platformList;
	private ScoreManager _theScoreManager;

	public DeathMenu TheDeathScreen;
	public PauseMenu ThePauseMenu;


	// Use this for initialization
	void Start () {
		_platformStartPoint = PlatformGenerator.position;
		_playerStartPoint = ThePlayer.transform.position;

		_theScoreManager = FindObjectOfType<ScoreManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape) && !TheDeathScreen.gameObject.active)
		{
			ThePauseMenu.PauseGame();
		}
	}
	
	public void RestratGame()
	{
		_theScoreManager.ScoreIncreasing = false;
		ThePlayer.gameObject.SetActive(false);
		
		TheDeathScreen.gameObject.SetActive(true);
		
		//StartCoroutine("RestartGameCo");
	}

	public void Reset()
	{
		TheDeathScreen.gameObject.SetActive(false);
		_platformList = FindObjectsOfType<ObjectDestroyer>();
		for (int i = 0; i < _platformList.Length; i++)
		{
			_platformList[i].gameObject.SetActive(false);
		}
		ThePlayer.transform.position = _playerStartPoint;
		PlatformGenerator.position = _platformStartPoint;
		ThePlayer.gameObject.SetActive(true);
		_theScoreManager.ScoreCount = 0;
		_theScoreManager.ScoreIncreasing = true;
	}
}
