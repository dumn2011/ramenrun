﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public string MainMenyLevel;

	public GameObject pauseMenu;

	public void PauseGame()
	{
		Time.timeScale = 0f;
		pauseMenu.SetActive(true);
	}

	public void UnpauseGame()
	{
		Time.timeScale = 1f;
		pauseMenu.SetActive(false);
	}

	public void RestartGame()
	{
		Time.timeScale = 1f;
		//SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		FindObjectOfType<GameManager>().Reset();
	}

	public void QuitToMainMenu()
	{
		Time.timeScale = 1f;
		SceneManager.LoadScene(MainMenyLevel);
	}
}
