﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroyer : MonoBehaviour {

	public GameObject PlatformDestructionPoint;
	
	// Use this for initialization
	void Start () {
		PlatformDestructionPoint = GameObject.Find("PlatformDestroyer");
	}
	
	// Update is called once per frame
	void Update () {

		if (transform.position.x < PlatformDestructionPoint.transform.position.x)
		{
			gameObject.SetActive(false);
		}
	}
}
