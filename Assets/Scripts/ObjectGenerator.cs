﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGenerator : MonoBehaviour {

	public GameObject ThePlatform;
	public Transform GenerationPoint;
	public float DistanceBetween;

	private float _platformWidth;

	public float DistanceBetweenMin;
	public float DistanceBetweenMax;

	public ObjectPooler[] TheObjectPools;
	
	private int _platformSelector;
	private float[] _platformWidths;

	private float _minHeight;
	private float _maxHeight;
	public Transform MaxHeightPoint;
	public float MaxHeightChange;
	private float _heightChange;

	private RamenGenerator _ramenGenerator;
	public float RamenProbability;
	
	public float RandomBarrierThreshold;
	public ObjectPooler BarrierPool;
	
	
	// Use this for initialization
	void Start () {
		_platformWidths = new float[TheObjectPools.Length];
		for (int i = 0; i < TheObjectPools.Length; i++)
		{
			_platformWidths[i] = TheObjectPools[i].pooledObject.GetComponent<BoxCollider2D>().size.x;
		}

		_minHeight = transform.position.y;
		_maxHeight = MaxHeightPoint.position.y;
		_ramenGenerator = FindObjectOfType<RamenGenerator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x<GenerationPoint.position.x)
		{
			DistanceBetween = Random.Range(DistanceBetweenMin, DistanceBetweenMax);
			
			_platformSelector = Random.Range(0, TheObjectPools.Length);

			_heightChange = transform.position.y + Random.Range(MaxHeightChange, -MaxHeightChange);

			if (_heightChange > _maxHeight)
			{
				_heightChange = _maxHeight;
			}else if (_heightChange < _minHeight)
			{
				_heightChange = _minHeight;
			}
			
			transform.position =
				new Vector3(transform.position.x + (_platformWidths[_platformSelector] / 2) + DistanceBetween,
					_heightChange, transform.position.z);

			//Instantiate(/*ThePlatform*/ TheObjectPools[_platformSelector], transform.position, transform.rotation);

			GameObject newPlatform = TheObjectPools[_platformSelector].GetPooledObject();

			newPlatform.transform.position = transform.position;
			newPlatform.transform.rotation = transform.rotation;
			newPlatform.SetActive(true);

			if (Random.Range(0f, 100f)<RamenProbability)
			{
				_ramenGenerator.SpawnRamen(new Vector3(transform.position.x, transform.position.y + 1f,
					transform.position.z));
			}
			
			if (Random.Range(0f, 100f)<RandomBarrierThreshold)
			{
				GameObject newSpike = BarrierPool.GetPooledObject();
				float spikeXPosition = Random.Range(-_platformWidths[_platformSelector] / 2 +1f,
					_platformWidths[_platformSelector] / 2 -1f);
				
				Vector3 spikePosition = new Vector3(spikeXPosition, 1f, 0f);
				
				newSpike.transform.position = transform.position + spikePosition;
				newSpike.transform.rotation = transform.rotation;
				newSpike.SetActive(true);
			}
			
			transform.position = new Vector3(transform.position.x + (_platformWidths[_platformSelector]/2),
				transform.position.y, transform.position.z);

		}
	}
}
