﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public float MoveSpeed;
	public float JumpForce;
	public float JumpTime;
	private float _jumpTime;

	private float _moveSpeedStore;
	public float SpeedMultiplier;
	public float SpeedIncreaseMilestone;
	private float _speedIncreaseMilestoneStore;
	private float _speedMilestouneCount;
	private float _speedMilestouneCountStore;
	private bool _stoppedJumping;
	
	private Rigidbody2D _myRigidbody2D;
	public bool Grounded;
	public LayerMask Ground;
	public Transform GroundCheck;
	public float Radius;

	private Animator _animator;
	
	public GameManager GameManager;

	// Use this for initialization
	void Start ()
	{
		_myRigidbody2D = GetComponent<Rigidbody2D>();
		_jumpTime = JumpTime;
		_animator = GetComponent<Animator>();
		_speedMilestouneCount = SpeedIncreaseMilestone;

		_moveSpeedStore = MoveSpeed;
		_speedMilestouneCountStore = _speedMilestouneCount;
		_speedIncreaseMilestoneStore = SpeedIncreaseMilestone;
		_stoppedJumping = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Grounded = Physics2D.OverlapCircle(GroundCheck.position, Radius, Ground);
		
		if (transform.position.x > _speedMilestouneCount)
		{
			_speedMilestouneCount += SpeedIncreaseMilestone;
			SpeedIncreaseMilestone = _speedMilestouneCount * SpeedMultiplier; 
			MoveSpeed *= SpeedMultiplier;
		}
		
		_myRigidbody2D.velocity = new Vector2(MoveSpeed, _myRigidbody2D.velocity.y);
		
		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (Grounded)
			{
				_myRigidbody2D.velocity = new Vector2(_myRigidbody2D.velocity.x, JumpForce);
				_stoppedJumping = false;
			}
		}
		
		if (Input.GetKey(KeyCode.Space) && !_stoppedJumping)
		{
			if (_jumpTime>0f)
			{
				_myRigidbody2D.velocity = new Vector2(_myRigidbody2D.velocity.x, JumpForce);
				_jumpTime -= Time.deltaTime;
			}
		}
		
		if (Input.GetKeyUp(KeyCode.Space))
		{
			_jumpTime = 0;
			_stoppedJumping = true;
		}
		if (Grounded)
		{
			_jumpTime = JumpTime;
		}
		
		_animator.SetFloat("Speed", _myRigidbody2D.velocity.x);
		_animator.SetBool("Grounded", Grounded);
	}
	
	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Death")
		{
			GameManager.RestratGame();
			MoveSpeed = _moveSpeedStore;
			_speedMilestouneCount = _speedMilestouneCountStore;
			SpeedIncreaseMilestone = _speedIncreaseMilestoneStore;
		}
	}
	
}
