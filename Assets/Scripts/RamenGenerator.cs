﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RamenGenerator : MonoBehaviour {
	
	public ObjectPooler RamenPool;

	public float DistanceBetweenRamen;
	
	public void SpawnRamen(Vector3 startPosition)
	{
		GameObject ramen1 = RamenPool.GetPooledObject();
		ramen1.transform.position = startPosition;
		ramen1.SetActive(true);
		
		GameObject ramen2 = RamenPool.GetPooledObject();
		ramen2.transform.position =
			new Vector3(startPosition.x - DistanceBetweenRamen, startPosition.y, startPosition.z);
		ramen2.SetActive(true);
		
		GameObject ramen3 = RamenPool.GetPooledObject();
		ramen3.transform.position =
			new Vector3(startPosition.x + DistanceBetweenRamen, startPosition.y, startPosition.z);
		ramen3.SetActive(true);
	}
}
