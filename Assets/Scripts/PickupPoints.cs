﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupPoints : MonoBehaviour {

	public int ScoreToGive;

	private ScoreManager _theScoreManager;

	
	// Use this for initialization
	void Start ()
	{
		_theScoreManager = FindObjectOfType<ScoreManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.name == "Player")
		{
			_theScoreManager.AddScore(ScoreToGive);
			gameObject.SetActive(false);
		}
	}
}
