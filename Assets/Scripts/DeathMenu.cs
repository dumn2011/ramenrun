﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathMenu : MonoBehaviour {

	public string MainMenyLevel;
	
	public void RestartGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	//	FindObjectOfType<GameManager>().Reset();
	}

	public void QuitToMainMenu()
	{
		SceneManager.LoadScene(MainMenyLevel);
	}
}
