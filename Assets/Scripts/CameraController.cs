﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public PlayerController ThePlayer;

	private Vector3 _playerPosition;
	private float _distanceToMove;
	
	// Use this for initialization
	void Start ()
	{
		ThePlayer = FindObjectOfType<PlayerController>();
		_playerPosition = ThePlayer.transform.position;

	}
	
	// Update is called once per frame
	void Update ()
	{
		_distanceToMove = ThePlayer.transform.position.x - _playerPosition.x;

		transform.position = new Vector3(transform.position.x + _distanceToMove, transform.position.y,
			transform.position.z);
		
		_playerPosition = ThePlayer.transform.position;
		
	}
}
